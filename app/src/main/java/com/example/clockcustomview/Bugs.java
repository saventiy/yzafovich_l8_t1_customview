package com.example.clockcustomview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.Calendar;

public class Bugs  extends View {

    private Path path = new Path();
    private Paint paint = new Paint();

    public Bugs(Context context) {
        super(context);
    }

    public Bugs(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public Bugs(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Calendar calendar = Calendar.getInstance();
        int seconds = calendar.get(Calendar.SECOND);

        drawSomeStuff(canvas, seconds);
        invalidate();

    }


    public void drawSomeStuff(Canvas canvas, int a){
        paint.setColor(Color.BLACK);
        paint.setAntiAlias(true);



        path.reset();

        path.moveTo(a * 10, 100);
        path.lineTo(200, 200);
        path.rLineTo(50, 200);

        canvas.drawPath(path, paint);

    }
}
