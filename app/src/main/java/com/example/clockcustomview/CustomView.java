package com.example.clockcustomview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import java.util.Calendar;

public class CustomView extends View {


    private Paint paint = new Paint();

    int left;
    int top;
    int right;
    int width;
    int height;
    int bottom;

    int porsion;

    int currentHour;
    int currentMinute;
    int currentSecond;

    public CustomView(Context context) {
        super(context);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(attrs);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

    }


    public void init(@Nullable AttributeSet set) {
        if (set == null)
            return;

        TypedArray typedArray = getContext().obtainStyledAttributes(set, R.styleable.CustomView);

        bottom = typedArray.getDimensionPixelSize(R.styleable.CustomView_height, bottom);

        typedArray.recycle();
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);


        width = getWidth();// getContext().getResources().getDisplayMetrics().widthPixels;
        height = getHeight();//getContext().getResources().getDisplayMetrics().heightPixels;

        porsion = width / 7;
        left = 0;
        right = porsion;
        bottom = height;
        top = height;

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        Calendar current = Calendar.getInstance();
        currentHour = current.get(Calendar.HOUR_OF_DAY) % 24;
        currentMinute = current.get(Calendar.MINUTE) % 60;
        currentSecond = current.get(Calendar.SECOND) % 60;


        canvas.translate(porsion, 0);
        paint.setColor(ContextCompat.getColor(getContext(), R.color.blue_900));
        drawColumn20(canvas, currentHour, "Hour");


        canvas.translate(porsion * 2, 0);
        paint.setColor(ContextCompat.getColor(getContext(), R.color.blue_700));
        drawColumn20(canvas, currentMinute, "Min");


        canvas.translate(porsion * 2, 0);
        paint.setColor(ContextCompat.getColor(getContext(), R.color.blue_500));
        drawColumn20(canvas, currentSecond, "Sec");


        invalidate();


    }


    private void drawColumn20(Canvas canvas, int givenHeight, String timeItem) {

        int top = (height - givenHeight * 10);
        int gap = 10;

        canvas.drawRect(new Rect(left, top, right, bottom), paint);

        paint.setColor(ContextCompat.getColor(getContext(), R.color.pink_900));
        paint.setTextSize(40);
        paint.isAntiAlias();
        paint.setTextAlign(Paint.Align.CENTER);


        canvas.drawText("" + givenHeight, (right - left) / 2, top - gap, paint);


        canvas.drawText(timeItem, (left + right) / 2, bottom / 4, paint);

    }


    public float pxFromDp(float dp) {
        return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                getResources().getDisplayMetrics()

        );
        //    (dp * getResources().getDisplayMetrics().density);
    }


}
