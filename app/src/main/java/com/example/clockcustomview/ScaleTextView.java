package com.example.clockcustomview;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class ScaleTextView extends LinearLayout {

    private TextView textViewQuestion;
    private CheckBox checkBoxYes;
    private CheckBox checkBoxNo;
    private int countShow;


    public ScaleTextView(Context context) {
        super(context);
        init();
    }

    public ScaleTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
        initAttr(attrs);
    }

    public ScaleTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
        initAttr(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ScaleTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
        initAttr(attrs);
    }


    private void initAttr(@Nullable  AttributeSet set){
        if(set == null)
            return;

        TypedArray typedArray = getContext().obtainStyledAttributes(set, R.styleable.ScaleTextView);

        countShow = typedArray.getDimensionPixelSize(R.styleable.ScaleTextView_show_count, 0);

        typedArray.recycle();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    private void init(){

        LayoutInflater.from(getContext()).inflate(R.layout.question_view, this);



        textViewQuestion = findViewById(R.id.text_view_question);
        checkBoxYes = findViewById(R.id.checkbox_yes);
        checkBoxNo = findViewById(R.id.checkbox_no);

        if(countShow == 1){
            checkBoxYes.setVisibility(GONE);
        } else if(countShow == 2){
            checkBoxYes.setVisibility(GONE);
            checkBoxNo.setVisibility(GONE);
        }



//        textViewQuestion.setVisibility(GONE);
//        checkBoxYes.setVisibility(GONE);
//        checkBoxNo.setVisibility(GONE);

//        textViewQuestion.setAlpha(0);
//        checkBoxYes.setAlpha(0);
//        checkBoxNo.setAlpha(0);


    }
}