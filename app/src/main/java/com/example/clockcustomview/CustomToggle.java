package com.example.clockcustomview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

public class CustomToggle extends View{


    private Paint paint = new Paint();

    float left;
    float top;
    float right;
    float width;
    float height;
    float bottom;

    float porsion;

    RectF rectF;


    public CustomToggle(Context context) {
        super(context);
    }

    public CustomToggle(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomToggle(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);


        width = getWidth();// getContext().getResources().getDisplayMetrics().widthPixels;
        height = getHeight();//getContext().getResources().getDisplayMetrics().heightPixels;

        porsion = width / 7f;
        left = 0f;
        right = porsion;
        bottom = height;
        top = height;


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        drawSomething(canvas);

    }



    private void drawSomething(Canvas canvas){


        paint.setStrokeWidth(10f);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);

        rectF = new RectF(right, right, width / 2 - right, height / 4 - right);

        int r = 40;
        canvas.drawRoundRect(rectF, r, r, paint);

        r = 1;
        canvas.drawCircle(((width / 2) - right),  right, r, paint);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:

            case MotionEvent.ACTION_UP:
                paint.setColor(Color.RED);
                invalidate();
            break;
        }

        return super.onTouchEvent(event);

    }
}
















